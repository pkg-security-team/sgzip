/*************************************************
         sgzlib - A seekable Gzip file format

   Author:         Michael Cohen (scudette@users.sourceforge.net)
   Version: 0.1   
   Copyright (2004).

   This library provides a unified interface for access and creation
   of sgzip files. sgzip files are files based on the gzip compression
   library which are also quickly seekable and therefore may be used
   for applications where seeking is important. See the .h file for a
   full description of this library.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.
                                                                                          
   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
                                                                                          
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
   USA
                                                                                          
************************************************/

#include "sgzlib.h"

#define DEFAULT_BLOCKSIZE 1024*1024/2


// Constant messages:
static char Malloc[]="Cant Malloc\n";
static char Write[]="Write Error - Could not write %s\n";
static char Read[]="Read Error - Could not read %s\n";

/* A fatal error occured */
void die(const char *message, ...)
{
	va_list ap;
	va_start(ap, message);
	vprintf(message, ap);
	va_end(ap);
	exit(-1);
};

/* A nonfatal error occured */
void warn(const char *message, ...)
{
	va_list ap;
	va_start(ap, message);
	vprintf(message, ap);
	va_end(ap);
};

/* Used for Debugging messages*/
void sgzip_debug(const char *message, ...)
{
	va_list ap;
	va_start(ap, message);
	vprintf(message, ap);
	va_end(ap);
};

/*
 Produces a default sgzip header. Mallocs its own memory, caller must
 free it.
*/
struct sgzip_header *sgzip_default_header(void) {
  struct sgzip_header *header;
  char magic[]="sgz";
  char compression[]="gzip";

  header=(struct sgzip_header *)malloc(sizeof(*header));
  if(!header) die(Malloc);
  memcpy(header->magic,magic,sizeof(magic));
  memcpy(header->compression,compression,sizeof(compression));
  header->blocksize=DEFAULT_BLOCKSIZE;
  return header;
};

/* Reads the header from the file. 
Returns NULL if the file can not be identified as a sgzip file 
*/
struct sgzip_header *sgzip_read_header(int fd) {
  struct sgzip_header *result;

  result=(struct sgzip_header *)malloc(sizeof(*result));
  if(!result)
    die(Malloc);
  lseek(fd,0,SEEK_SET);

  if(read(fd,result,sizeof(*result))<sizeof(*result)) {
    die(Read,"header");
  };
  
  if(strncmp(result->magic,"sgz",3)) {
    warn("File does not look like a sgz file\n");
    return(NULL);
  };

  return(result);
};

/* Write a correct file header on the file descriptor.

   The user can pass in a prefilled in struct sgzip_header *, in which
   case a new struct will not be malloced. If the user passes in NULL,
   a new struct will be malloced and returned.
 */
struct sgzip_header *sgzip_write_header(int fd,struct sgzip_header *header) {
  if(!header){
    header=sgzip_default_header();
  }
  
  if(write(fd,header,sizeof(*header))<sizeof(*header)) 
    die(Write,"header");

  return(header); 
};


struct sgzip_index_list *add_item(struct sgzip_index_list *index,int block_length) {
  struct sgzip_index_list *i;

  //i is the last item in the list:
  for(i=index;i && i->next;i=i->next);

  if(!index) {
    index=(struct sgzip_index_list *)malloc(sizeof(*index));
    if(!index) die(Malloc);

    i=index;
    i->offset=block_length;
    i->next=NULL;
  } else {
    i->next=(struct sgzip_index_list *)malloc(sizeof(*i->next));
    if(!i->next) die(Malloc);
    
    /* 
       We store a cumulative absolute offset in our linked lists and
       file, but we only really need to store a relative
       offset. Relative offsets can save 2 bytes per block, but lose
       the ability to recover from a corrupted file (although it
       could be difficult to resync anyway if the file is corrupted
       - so it may not be practical to recover from a corrupted
       sgzip file).
    */
    i->next->offset=i->offset+block_length;
    i=i->next;
    i->next=NULL;
  };

  return(index);
};

void sgzip_write_index(int outfd,unsigned long long int *index) {
  int j,count=0;

  for(j=0;index[j];j++) {
    write(outfd,index+j,sizeof(*index));
    count++;
  }

  //Write the size of the index
  write(outfd,&count,sizeof(count));

  //Write the index magic to indicate this file has an index:
  if(write(outfd,"sgzidx",sizeof(char)*6)<6) 
    warn("Could not write index magic\n");
};

/*
This function reads from the fd, until the buffer is full. If a read
does not return enough bytes to fill the buffer (as would happen if we
read from a socket or pipe), we retry again.

If however, the read returns no bytes (and its blocking) then we
assume that the file is finished, and return a short buffer.

If we fail to read, we return the error code back.
*/
int read_from_stream(int fd,char *buf,int length) {
  int result;
  char *current_p = buf;
  
  while(length>0) {
    if(length==0) 
      break;

    result=read(fd,current_p,length);
    if(result<0) { //Error occured
      return(result);
    } else if(result==0) { //EOF reached
      break;
    };
    length-=result;
    current_p+=result;
  };
  return(current_p-buf);
};


/* Copy stream in to stream out */
void sgzip_compress_fds(int infd,int outfd,const struct sgzip_header *header) {
  char *datain;
  unsigned int lengthin;
  char *dataout;
  unsigned int lengthout;
  int result;
  struct sgzip_index_list *index_list=NULL;
  struct sgzip_index_list *i;
  unsigned int count=0;
  unsigned long long int *index;

  datain=(char *) malloc(header->blocksize);
  dataout=(char*)malloc(header->blocksize+1024);
  if(!datain || !dataout)
    die(Malloc);

  do {
    //Read a block from the input
    lengthin=read_from_stream(infd,datain,sizeof(*datain)*header->blocksize);
    if(lengthin<0) {
      warn("Error reading from file descriptor\n");
      return;
    };

    //Compress this block
    lengthout=header->blocksize+1024;
    result = compress(dataout,(long int *)&lengthout,datain,(long int)lengthin);
    if(result!=Z_OK) {
      warn("Cant compress block of size %lu into size %lu...\n" , lengthin, lengthout);
    };

    //Now we write the size of the compressed buffer as a pointer to the next buffer.
    if(write(outfd,&lengthout,sizeof(lengthout))<sizeof(lengthout))
      warn("Cant write block size\n");
    
    //Add this to the index list:
    index_list=add_item(index_list,lengthout);

    //And the compressed data:    
    result=write(outfd,dataout,sizeof(*dataout)*lengthout);
    if(result<lengthout) {
      warn("Underwrote this buffer - only wrote %u/%u bytes\n",result,lengthout);
    };

    count++;
  } while(lengthin>0);

  //Write a single int of zero offset to indicate the blocks have finished:
  lengthout=0;
  result=write(outfd,&lengthout,sizeof(lengthout));

  //Now write the index to the file:
  index=(unsigned long long int *)calloc(sizeof(*index),count+1);
  if(!index) die(Malloc);
  
  for(count=0,i=index_list;i;i=i->next,count++) {
    index[count]=i->offset;
  };  
  index[count]=0;

  sgzip_write_index(outfd,index);

  free(index);
  free(datain);
  free(dataout);
};

/* read a random buffer from the sgziped file */
int sgzip_read_random(char *buf, int len, unsigned long long int offs,
		int fd, unsigned long long int *index,const struct sgzip_header *header) {
  char *data,*temp;
  unsigned int block_offs,clength,length,copied=0,buffer_offset,available;
  int result;

  data=(char *)malloc(sizeof(*data)*(header->blocksize+1024));
  temp=(char *)malloc(sizeof(*temp)*(header->blocksize+1024));
  if(!data || !temp) die(Malloc);

  block_offs=(int)(offs/header->blocksize);

  //The offset where we need to start from in the individual block.
  buffer_offset=offs % header->blocksize;

  while(len>0) {
    //Length of this block
    clength=index[block_offs+1]-index[block_offs];

    if(clength>=header->blocksize) {
      perror("Clength is too large");
    };

    //Read the compressed block from the file:
    if(
       lseek(fd,sizeof(struct sgzip_header)+sizeof(unsigned int)*(block_offs+1)+index[block_offs],SEEK_SET)<0 ||
       read(fd,data,clength) <0
       ) {
      perror("Compressed file reading problem");
    };
  
    length=header->blocksize;
    result=uncompress(temp,(long int *)&length,data,clength);
    if(!result==Z_OK) {
      warn("Cant decompress block %lu \n" , block_offs);
      free(data);
      free(temp);
      return(-1);
    };
  
    
    //The available amount of data to read:
    available=header->blocksize-buffer_offset;
    if(available>len) {
      available=len;
    };

    //Copy the right data into the buffer
    memcpy(buf+copied,temp+buffer_offset,available);
    len-=available;
    copied+=available;
    block_offs++;
    buffer_offset=0;
  }

  free(data);
  free(temp);
  return(copied);
} ;

/* This function reads the index from the file and returns an array of
   long ints representing the offsets into the compressed image where
   the blocks can be found. Each entry in the array is blocksize big,
   so to seek to an arbitrary location, we just divide the location by
   the blocksize and use that as the reference to the correct block in
   the file. 

   If the index is not there we flag an error by returning null.
*/
unsigned long long int *sgzip_read_index(int fd) {
  char magic[6];
  unsigned int count;
  unsigned long long int *index;
  unsigned long long int end;

  //Find the end of file:
  end=lseek(fd,0,SEEK_END);
  if(end<0) return(NULL);

  //First we detect if there is an index at the end by reading the magic
  if(lseek(fd,(unsigned long long int)(end-6-sizeof(count)),SEEK_SET)<0) {
    /* This file may not be seekable, in this case we cant read its
       index.  We can rebuild the index from the file itself, but a
       non-seekable file cannot be used for read_random, and simply
       decompressing it in a stream does not need an index.
    */
    return(NULL);
  };
  
  if(read(fd,&count,sizeof(count))<sizeof(count))
    die(Read,"index");

  if(read(fd,magic,sizeof(char)*6)<6)
    die(Read,"index");

  if(strncmp(magic,"sgzidx",6)) {
    warn("It appears that there is no index on this file, you may regenerate the index\n");
    return(NULL);
  };

  //Allocate enough memory for the array:
  index=(unsigned long long int *)calloc(count+1,sizeof(*index));
  if(!index) 
    die(Malloc);

  //Now find the start of the index
  if(lseek(fd,end-6-sizeof(count)-count*sizeof(*index),SEEK_SET)<0) {
    warn("seek error\n");
    return(NULL);
  };

  //Read the array in:
  if(read(fd,index+1,sizeof(*index)*count)<sizeof(*index)*count) {
    warn("Unable to read the index\n");
    return(NULL);
  };

  //Null terminate the end of the array
  index[count]=0;
  return(index);
};

/* 
   reads the stream and calculates the index map.

   This is done by following all the blocks throughout the file and
   rebuilding the index. None of the blocks are decoded so this should be quick.
   We return an index array of unsigned long long ints.
 */
unsigned long long int *sgzip_calculate_index_from_stream(int fd,const struct sgzip_header *header) {
  int length=0;
  int zoffset=0;
  int offset=0,count=0;
  char *datain;
  int datalength=header->blocksize+1024;
  struct sgzip_index_list *result=NULL,*i;
  unsigned long long int *index=NULL;

  /* We use read, rather than seek so this will work on non-seekable streams. */
  datain=(char *) malloc(datalength);
  if(!datain) 
    die(Malloc);
  
  while((read(fd,&length,sizeof(length))>0) && length>0){
    count++;
    /* If we need to read more data than we expected we bail because
       the blocksize specified in the header is incorrect */
    if(length>datalength) {
      die("blocksize %lu is bigger than that specified in the header, invalid sgzip file\n",length);
    };

    zoffset+=length;

    // Add to linked list
    result=add_item(result,length);

    //Seek length bytes from here for the next value
    if(read(fd,datain,sizeof(*datain)*length)<length) {
      die(Read,"file");
      return(NULL);
    };
    offset+=header->blocksize;
  };

  //Create an index table:
  index=(unsigned long long int *)malloc(count*sizeof(*index));
  if(!index) die(Malloc);

  count=1;
  for(i=result;i;i=i->next) {
    index[count]=i->offset;
    count++;
  };

  index[count]=0;
  return(index);
};

/* 
   Decompress the stream, writing it into the outfd.
 */
void sgzip_decompress_fds(int fd,int outfd,const struct sgzip_header *header) {
  int length=0,lengthout=0,result;
  int zoffset=0;
  int count=0;
  char *datain,*dataout;
  int datalength=header->blocksize+1024;

  /* We use read, rather than seek so this will work on non-seekable streams. */
  datain=(char *) malloc(datalength);
  dataout=(char *) malloc(datalength);
  if(!datain || !dataout) 
    die(Malloc);
  
  while((read(fd,&length,sizeof(length))>0) && length>0){
    count++;
    /* If we need to read more data than we expected we bail because
       the blocksize specified in the header is incorrect */
    if(length>datalength) {
      die("blocksize %lu is bigger than that specified in the header, invalid sgzip file\n",length);
    };

    zoffset+=length;

    if(read(fd,datain,sizeof(*datain)*length)<length) {
      die(Read,"file");
    };
    
    //Now uncompress this block:
    lengthout=datalength;
    result=uncompress(dataout,(long int *)&lengthout,datain,length);
    if(result!=Z_OK) {
      warn("Cant compress block of size %lu into size %lu..., filling with zeros\n" , length, datalength);
      memset(dataout,0,datalength);
    };
    
    //Write the output:
    if(write(outfd,dataout,lengthout)<lengthout) {
      die(Write,"to decompress file");
    };
  };

  free(datain);
  free(dataout);
};
