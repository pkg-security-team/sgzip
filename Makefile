LDFLAGS=-lz
CFLAGS=-g -O0 -Wall
CPPFLAGS= -DDEBUG


sgzip: sgzip.o sgzlib.o

sgzlib.o: sgzlib.c sgzlib.h

clean:
	rm -f *.o sgzip

